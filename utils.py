##Python Utilities
##Anubhav Kumar
##April 28th 2019

import Queue
import datetime
import threading

class __LogPOPO:
	def __init__(self):
		self.time = datetime.datetime.now()
		self.logMessage = ""
		self.logCode = ""
	
class Utils:
	def __init__(self):
		self.LoggerTrigger = False
		self.LogQueue = Queue.Queue(maxsize = 20)
		
	def GetParameterFromConfigFile(self, parameterName,parameterBlockName=""):
		import configparser
		config = configparser.ConfigParser()
		config.read("App.Config")
		if parameterBlockName != "" and parameterBlockName in config.sections():
			return config[parameterBlockName][parameterName]
		else:
			return config["Default"][parameterName]

	def RunStoredProcedure(self, spName, parametersList):
		import mysql.connector
		from mysql.connector import Error
		from mysql.connector import errorcode
		returnResult = None
		print parametersList
		try:
			Host=self.GetParameterFromConfigFile("host","Database")
			DatabaseName=self.GetParameterFromConfigFile("DatabaseName","Database")
			UserName=self.GetParameterFromConfigFile("DatabaseUserName","Database")
			Password=self.GetParameterFromConfigFile("DatabasePassword","Database")
			mySQL_conn = mysql.connector.connect(host=Host,
										   database=DatabaseName,
										   user=UserName,
										   password=Password)
			cursor = mySQL_conn.cursor()
			cursor.callproc(spName,parametersList)
			mySQL_conn.commit()
			for result in cursor.stored_results():
				returnResult = (result.fetchall())
			
		except mysql.connector.Error as error:
			print("Failed to execute stored procedure: {}".format(error))
		finally:
			# closing database connection.
			if (mySQL_conn.is_connected()):
				cursor.close()
				mySQL_conn.close()
			return returnResult
	
	def __WriteLog(self, code, message):
		if not self.LoggerTrigger:
			return "Log Trigger is OFF"
		LogMessage = __LogPOPO
		LogMessage.logCode = code
		LogMessage.logMessage = message
		self.LogQueue.put(LogMessage)
		return True
	
	def TriggerLoggerON(self):
		self.LoggerTrigger = True
		LoggerThread = threading.Thread(target=self.__LoggingProcess)
		LoggerThread.start()
		
	def WriteInfo(self, message):
		self.__WriteLog("Info", message)
	
	def WriteError(self, message):
		self.__WriteLog("Info", message)
		
	def __LoggingProcess(self):
		FileOpenBool = False
		while(self.LoggerTrigger or not self.LogQueue.empty()):
			if not self.LogQueue.empty():
				if not FileOpenBool:
					FileObject = open(self.GetParameterFromConfigFile(Logfilename,Logging), "a")
					FileOpenBool = True
				__LogPOPO = self.LogQueue.get()
				data = (__LogPOPO.logCode, datetime.datetime.now(), __LogPOPO.logMessage)
				FileObject.write("[%s] : %s : %s \n" % data)
			else:
				if FileOpenBool :
					FileObject.close()
			
	def MakeHTTPGetRequest(self, URL, parameters):
		import request
		r = requests.get(url = URL, params = PARAMS) 
  		data = r.json()
		htmlsourcecode = r.text()
		return [data,htmlsourcecode]
	
	def MakeHTTPPostRequest(self, URL, parameters):
		import request
		r = requests.post(url = URL, params = PARAMS) 
  		data = r.json()
		htmlsourcecode = r.text()
		return [data,htmlsourcecode]
	
utils = Utils()